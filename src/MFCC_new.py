import os
import numpy as np
import scipy
from scipy.io import wavfile
import scipy.fftpack as fft
from scipy.signal import get_window
import IPython.display as ipd
import matplotlib.pyplot as plt
import pandas as pd


pre_emphasis = 0.97 # pre-emphasizing coefficient, y(t) = x(t) - ax(t-1)
frame_size = 0.025 # we are using 25ms frame size for MFCC  
frame_stride = 0.01 #10 ms stride,frame overlap
colnames = ['mfcc1','mfcc2','mfcc3','mfcc4','mfcc5','mfcc6','mfcc7','mfcc8','mfcc9','mfcc10','mfcc11','mfcc12','lb']
# Creating an empty Dataframe with column names only
df = pd.DataFrame(columns=colnames)
#44.1KHZ is standart sampling rate for audio files

mydir = os.getcwd() +'\dataset'

NFFT = 512 # deafult size is either N=256 or 512 for N-point FFT 
nfilt = 40 #40 filtering banks
num_ceps = 12 #number of cepstrum coefficients seleccted for mfcc
cep_lifter = 22 #sinusoidal liftering to improve mfcc even for noisy signals
low_freq_mel = 0 # lowest mel scale value

for root, dirs, files in os.walk(mydir):
    for subfiles in files: #[:5]:
        currentFile=os.path.join(root, subfiles)
        if currentFile.endswith(".wav"):
            #load audio
            audio, signal = scipy.io.wavfile.read(currentFile) 
            #Signal emphisizing help to amplify high frequencies 
            # y(t) = x(t) - ax(t-1) where a= 0.97
            EmpSignal = np.append(signal[0], signal[1:] - pre_emphasis * signal[:-1])
            # calculating Frame lengths, frame steps, and rounding to avoid numerical errors 
            frame_len, frame_step = frame_size * audio, frame_stride * audio  
            signal_length = len(EmpSignal)
            frame_len = int(round(frame_len))
            frame_step = int(round(frame_step))
            num_frames = int(np.ceil(float(np.abs(signal_length - frame_len)) / frame_step))  
           
            # zero-padding to increase equal time of length for FFT
            pad_signal_length = num_frames * frame_step + frame_len
            z = np.zeros((pad_signal_length - signal_length))
            pad_signal = np.append(EmpSignal, z) 
            #giving indices of each frames to know each frame's start-finishing points
            indices = np.tile(np.arange(0, frame_len), (num_frames, 1)) + np.tile(np.arange(0, num_frames * frame_step, frame_step), (frame_len, 1)).T
            frames = pad_signal[indices.astype(np.int32, copy=False)]
            
            #applying hamming windowing using numpy.hamming function
            frames *= np.hamming(frame_len)
            
            #N-point FFT for frames, magnitude of FTT
            mag_frames = np.absolute(np.fft.rfft(frames, NFFT))  
            #Power Spectrum
            pow_frames = ((1.0 / NFFT) * ((mag_frames) ** 2))  
            
            #Filter Bank calculation
            high_freq_mel = (2595 * np.log10(1 + (audio / 2) / 700))  # Convert Hz to Mel
            # Equally spaced in Mel scale
            mel_points = np.linspace(low_freq_mel, high_freq_mel, nfilt + 2)  
            # Convert Mel to Hz
            hz_points = (700 * (10**(mel_points / 2595) - 1))  
            bin = np.floor((NFFT + 1) * hz_points / audio)

            fbank = np.zeros((nfilt, int(np.floor(NFFT / 2 + 1))))
            for m in range(1, nfilt + 1):
                f_m_minus = int(bin[m - 1])   # left
                f_m = int(bin[m])             # center
                f_m_plus = int(bin[m + 1])    # right

                for k in range(f_m_minus, f_m):
                    fbank[m - 1, k] = (k - bin[m - 1]) / (bin[m] - bin[m - 1])
                for k in range(f_m, f_m_plus):
                    fbank[m - 1, k] = (bin[m + 1] - k) / (bin[m + 1] - bin[m])
            filter_banks = np.dot(pow_frames, fbank.T)
            filter_banks = np.where(filter_banks == 0, np.finfo(float).eps, filter_banks)  
            filter_banks = 20 * np.log10(filter_banks)  # 
            
            # at last, number of cepstrals as given for receiveing 12 coefficient for mfcc
            mfcc = scipy.fft.dct(filter_banks, type=2, axis=1, norm='ortho')[:, 1 : (num_ceps + 1)] 
            (nframes, ncoeff) = mfcc.shape
            n = np.arange(ncoeff)
            # liftering on mfcc..
            lift = 1 + (cep_lifter / 2) * np.sin(np.pi * n / cep_lifter)
            mfcc *= lift  
            # we substract mean value from both filtering banks and mfcc's to normalize and increase SNR ratio
            filter_banks -= (np.mean(filter_banks, axis=0) + 1e-8)
            mfcc -= (np.mean(mfcc, axis=0) + 1e-8)
            # after having matrix of frames x mfcc values mean value of frames substracted to classify 
            #note: it turns out this approach is not a good idea, we will use other methods such as bag-of-words for classification 
            mfcc_vec= mfcc.mean(axis=0).tolist()
            #adding song-name for label column into vector
            mfcc_vec.append(os.path.basename(root))
            
            # appending mfcc vector into dataframe
            dfObj = pd.Series(mfcc_vec, index = df.columns)
            df = df.append(dfObj, ignore_index=True)
#exporting dataframe into cvs file...
df.to_csv(os.getcwd()+'\\Features_out_with1minSamples.csv', index = False,header=False)

# Citation: 
# Haytham M. Fayek,Speech Processing for Machine Learning: Filter banks, Mel-Frequency Cepstral Coefficients (MFCCs) and What's In-Between: https://haythamfayek.com/2016/04/21/speech-processing-for-machine-learning.html"          
        
