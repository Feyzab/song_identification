"""
    Implementation of K Nearest Neighbors

    Author: Phong Hai Do - Student ID: 309047
"""

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

from knn_algorithm import KNearestNeighbors
import random
import time


if __name__ == "__main__":
    
    # Read Iris data into a list
    data_training = pd.read_csv('src/songs_splitted.csv')
    data_training = pd.DataFrame(data_training)
    data_training = data_training.values.tolist()

    # for each data sample in the dataset
    # change the label from string to integer
    for data_sample in data_training:
        if data_sample[-1] == 'home-michael_buble':
            data_sample[-1] = 0
        elif data_sample[-1] == 'love_yourself-justin_bieber':
            data_sample[-1] = 1
        elif data_sample[-1] == 'perfect-ed_sheeran':
            data_sample[-1] = 2
        elif data_sample[-1] == 'say_you_wont_let_go-james_arthur':
            data_sample[-1] = 3
        elif data_sample[-1] == 'thinking_out_loud-ed_sheeran':
            data_sample[-1] = 4

    # number of folder that will be used for cross-training
    number_folder = 5

    # the accuracy after the cross-training
    actual_accuracy = 0

    start_time = time.time()

    scaler = StandardScaler()
    
    # for each times of the cross-training
    for times in range(0, number_folder):
        # shuffle the dataset randomly
        random.shuffle(data_training)

        # the variable to store feature values in feature vectors
        features = list()
        # the variable to store labels in feature vectos
        labels = list()

        # for each data sample in the dataset, get the feature values and the labels
        for data_sample in data_training:
            features.append(data_sample[:-1])
            labels.append(data_sample[-1])
        
        # normalize features of dataset
        # features = preprocessing.scale(features)
        features = scaler.fit_transform(features)
        
        # split the dataset into training set and validation set
        x_train, x_validate, y_train, y_validate = train_test_split(features, labels, test_size=0.2)

        # Initilize the KNN classifier with k is the number of the nearest neighbors
        classifier = KNearestNeighbors(k=10)
        classifier.fit(x_train, y_train)

        # predict the label in validation set
        predictions = classifier.predict(x_validate)

        # add the accuracy of this cross-training to the total accuracy
        actual_accuracy = actual_accuracy + accuracy_score(y_validate, predictions)
    
    end_time = time.time()
    # calculate the actual accuracy after cross-training
    print('Actual accuracy is: %.2f %%' % (actual_accuracy * 100 / number_folder))
    print('Running time: %.2f (s)' % (end_time - start_time))
