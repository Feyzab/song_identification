"""
    Implementation of K Nearest Neighbors

    Author: Phong Hai Do - Student ID: 309047
"""

import numpy as np
import operator


def euclidean_distance(sample1, sample2):
    """
    Calculate Euclidean distance between two data points
    """
    return np.sqrt(np.sum((np.array(sample1) - np.array(sample2))**2))

class KNearestNeighbors():
    """
    Initilize KNearstNeighbors Class with k is the number of neighbors
    """
    def __init__(self, k):
        super().__init__()
        self.k = k
    
    """
    Store the training data into x_train
    and its lavels into y_train
    """
    def fit(self, x_train, y_train):
        self.x_train = x_train
        self.y_train = y_train

    """
    Predict the labels for testing set
    The output will be stored into predictions variable
    """
    def predict(self, x_test):
        predictions = list()

        # for each sample test in the testing set
        for sample_test in x_test:
            list_distances = list()

            # calculate the euclidean distance between the new sample testing
            # and every sample in the training set
            for index, sample_train in enumerate(self.x_train):
                distance = euclidean_distance(sample_test, sample_train)

                # an element of list_distance contains:
                # feature vector of the sample test
                # euclidean distance between this sample test and a sample in the training set
                # the index of the sample in the training set that is used to calculate the distance with the sample test
                list_distances.append((sample_test, distance, index))
            
            # sort the distance in ascending order
            # to find k smallest value of distance (k nearest neighbors) 
            list_distances.sort(key = lambda tuple:tuple[1])
            nearest_neighbors = list_distances[:self.k]

            # take the index of labels of the k nearest neighbors
            list_index_labels = [label[-1] for label in nearest_neighbors]
            list_labels = list()

            # find the label of all k nearest neighbors in the training set
            for index in list_index_labels:
                list_labels.append(self.y_train[index])
            
            '''
            Use weight function
            '''

            # declare the variable that store value of predicted label
            predicted_label = -1

            # the variable that store value of weight for each label
            weight = [0] * 5

            # loop over all the nearest neighbors
            for index in range(len(nearest_neighbors)):
                
                # get the lable of the current nearest neighbor 
                label = list_labels[index]

                # check the distance between the current nearest neighbor and the sample test
                # if the value of distance is not zero
                # calculate the weight value for this current nearest neighbor
                # if the value of distance is equal to zero
                # then the value of predicted label will be equal to the label of this current nearest neighbor
                if nearest_neighbors[index][1] != 0:
                    weight[label] += (1 / nearest_neighbors[index][1])
                else:
                    predicted_label = label
            
            # if the value of predicted label is less than zero (which means the value was not set)
            # the value of predicted label will be calculated by finding the label has maximum value of weight
            if predicted_label < 0:
                predicted_label, _value = max(enumerate(weight), key=operator.itemgetter(1))
            
            
            '''
            Use major voting
            '''
            # count the number of labels to find the most frequently occurring label
            # predicted_label = max(set(list_labels), key=list_labels.count)
            # add the predicted label of the current sample test to the list of predicted labels

            # append value of predicted label to predictions
            predictions.append(predicted_label)
        
        return predictions

