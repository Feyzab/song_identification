# the code is used for neccessary mp3 to wav conversion needed for feature extraction:
#the code and dataset folder must be in the same directory
import os 
from pydub import AudioSegment
import math
    
#dataset file is considered as same dir as this code
mydir = os.getcwd() +'\dataset'
print(mydir)
for root, dirs, files in os.walk(mydir):
    for subfiles in files:
        currentFile=os.path.join(root, subfiles)
        if currentFile.endswith(".mp3"):
            src=currentFile
            nameofmp3 = os.path.basename(currentFile)
            #dst=os.path.join(mydir,nameofmp3 + '.wav')
            dst=os.path.splitext(currentFile)[0] + '.wav' #for converting in same subfolder 
            #convert wav to mp3                                                            
            sound = AudioSegment.from_mp3(src)
            sound.export(dst, format="wav")
            split_wav = AudioSegment.from_wav(dst) 
            total_mins = math.ceil(split_wav.duration_seconds / 60)
            t1= 0 
            t2= 0
            for i in range(0, total_mins,1): #in every 1min
                split_wav = AudioSegment.from_wav(dst) 
                split_filename = os.path.splitext(dst)[0]+ '_'+str(i)+ '.wav'
                t1 = 60000 * (i)
                t2 = 60000 * (i+1)
                split_wav = split_wav[t1:t2]
                split_wav.export(split_filename, format="wav")    
            os.remove(dst)
            os.remove(src)

            
            
